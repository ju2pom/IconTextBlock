﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ShapeDemo
{
  public class TextShape : Shape
  {
    public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
      "Text",
      typeof(string),
      typeof(TextShape),
      new PropertyMetadata(default(string), OnTextChanged));

    private static void OnTextChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
    {
      TextShape textShape = (TextShape)dependencyObject;

      textShape.texts = textShape.Text.Split(',').Select(textShape.ConvertTextToTypeFace).ToArray();

      textShape.InvalidateVisual();
    }

    public string Text
    {
      get { return (string) this.GetValue(TextProperty); }
      set { this.SetValue(TextProperty, value); }
    }

    private Geometry geometry;
    private FormattedText[] texts;

    protected override Geometry DefiningGeometry => geometry ?? (geometry = BuildGeometry());

    protected override Size MeasureOverride(Size constraint)
    {
      return this.DefiningGeometry.Bounds.Size;
    }

    protected override Size ArrangeOverride(Size finalSize)
    {
      return this.DefiningGeometry.Bounds.Size;
    }

    protected override void OnRender(DrawingContext drawingContext)
    {
      foreach (FormattedText text in this.texts)
      {
        drawingContext.DrawText(text, new Point());
      }
   }

    private FormattedText ConvertTextToTypeFace(string text)
    {
      var strings = text.Split(':');
      var character = strings[0];
      var color = new SolidColorBrush((Color)ColorConverter.ConvertFromString(strings[1]));
      var fontFamily = new FontFamily(strings[2]);
      var typeface = new Typeface(fontFamily, FontStyles.Normal, FontWeights.Normal, new FontStretch());

      double textHeight;
      double.TryParse(strings[3], out textHeight);

      return new FormattedText(character, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, typeface, textHeight, color);
    }

    private Geometry ConvertTextToGeometry(string text)
    {
      var formattedText = this.ConvertTextToTypeFace(text);

      return formattedText.BuildGeometry(new Point());
    }

    private Geometry BuildGeometry()
    {
      var geometries = Text.Split(',').Select(ConvertTextToGeometry);

      var group = new GeometryGroup();
      geometries.ToList().ForEach(group.Children.Add);

      return group;
    }
  }
}